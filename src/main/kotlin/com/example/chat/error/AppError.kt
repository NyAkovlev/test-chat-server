package com.example.chat.error

import com.example.chat.domain.model.db.chat.ChatPermission
import com.example.chat.domain.model.db.user.Permission
import graphql.GraphQLException

sealed class AppError(
        val case: AppErrorCase,
        val description: String? = null
) : Error(description ?: case.msg) {
    class AuthenticationRequired : AppError(AppErrorCase.AuthenticationRequired)
    class InvalidParameters(description: String? = null) : AppError(AppErrorCase.InvalidParameters, description)
    class InvalidAuth(description: String? = "Invalid username or password") : AppError(AppErrorCase.InvalidParameters, description)
    class TokenException() : AppError(AppErrorCase.TokenException, "The JWT token is expired or invalid")
    class ModelNotFound(type: String) : AppError(AppErrorCase.ModelNotFound, "a model $type not found")
    class ModelIdAlreadyExist(description: String? = null) : AppError(AppErrorCase.ModelIdAlreadyExist, description)
    class HaveNotPermission(permission: Permission) : AppError(AppErrorCase.UserHaveNotPermission, permission.name)
    class HaveNotChatPermission(permission: ChatPermission) : AppError(AppErrorCase.UserHaveNotPermission, permission.name)
    class AccessDenied(description: String? = null) : AppError(AppErrorCase.AccessDenied, description)
    fun toGraphQL(e: Throwable? = null): GraphQLException {
        return GraphQLException("[${this.case.ordinal}]:${this.case.msg} ${this.description ?: ""}", e ?: this)
    }
}

