package com.example.chat.error

enum class AppErrorCase(val msg: String) {
    AuthenticationRequired("Authentication required"),
    InvalidParameters("Invalid input parameters"),
    TokenException("The token is expired or invalid"),
    ModelNotFound("Model not found"),
    ModelIdAlreadyExist("Model's id already exist"),
    UserHaveNotPermission("User don't have permission"),
    AccessDenied("access denied"),
}