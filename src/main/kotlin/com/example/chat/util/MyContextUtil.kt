package com.example.chat.util

import com.example.chat.config.security.auth.SecurityUserDetails
import com.example.chat.domain.model.db.user.Permission
import com.example.chat.domain.model.db.user.UserModel
import com.example.chat.error.AppError
import org.springframework.security.core.Authentication
import org.springframework.web.context.request.RequestContextHolder
import org.springframework.web.context.request.ServletRequestAttributes
import javax.servlet.http.HttpSession

class MyContextUtil {
    companion object {
        const val securityContext = "SPRING_SECURITY_CONTEXT_AUTHENTICATION"
        private fun getSession(): HttpSession {
            return (RequestContextHolder.currentRequestAttributes() as ServletRequestAttributes)
                    .request
                    .getSession(true)
        }

        private fun HttpSession.getAuthentication(): Authentication? {
            return getAttribute(securityContext) as Authentication?
        }

        fun getUserOrNull(permission: Set<Permission>? = null): UserModel? {
            return try {
                getUser(permission)
            } catch (e: Throwable) {
                null
            }
        }

        fun getUser(permission: Set<Permission>? = null): UserModel {
            val authentication = getSession().getAuthentication()
            val principal = authentication?.principal
            if (authentication != null && authentication.isAuthenticated && principal is SecurityUserDetails) {
                val user = principal.userModel
                if (permission != null) {
                    val userPermissions = user.role.permissions
                    for (item in permission) {
                        if (!userPermissions.contains(item)) {
                            throw AppError.HaveNotPermission(item)
                        }
                    }
                }
                return user
            } else {
                throw AppError.AuthenticationRequired()
            }
        }

        fun HttpSession.setAuthentication(value: Authentication?) {
            return setAttribute(MyContextUtil.securityContext, value)
        }

    }

}


