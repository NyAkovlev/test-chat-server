package com.example.chat.util

import com.example.chat.config.security.auth.SecurityUserDetails
import com.example.chat.domain.model.db.user.UserModel
import com.example.chat.error.AppError
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import java.security.Principal

class WebSocketUtil {
    companion object {

        fun Principal.getUser(): UserModel {
            return ((this as? UsernamePasswordAuthenticationToken)?.principal as? SecurityUserDetails)?.userModel
                    ?: throw AppError.AuthenticationRequired()
        }
    }
}