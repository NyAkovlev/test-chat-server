package com.example.chat.util

import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZonedDateTime

fun LocalDateTime.toTimeZoned(): ZonedDateTime {
    return ZonedDateTime.of(this, ZoneId.systemDefault())
}

fun ZonedDateTime.toDate(): LocalDateTime {
    return LocalDateTime.from(this.toInstant())
}