package com.example.chat.api.rest

import com.example.chat.domain.model.api.FileResponse
import com.example.chat.domain.worker.FileWorker
import com.example.chat.util.MyContextUtil
import org.springframework.core.io.FileSystemResource
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile


@RestController
@RequestMapping("api/files")
class FileRestController(private val fileWorker: FileWorker) {

    @PostMapping(consumes = [MediaType.MULTIPART_FORM_DATA_VALUE], path = ["/upload"])
    fun upload(
            @RequestPart(name = "file") file: MultipartFile,
            @RequestPart(name = "uuid") uuid: String,
            @RequestPart(name = "name") name: String
    ): ResponseEntity<*>? {
        val user = MyContextUtil.getUser()
        val fileModel = fileWorker.save(user, file, uuid, name)
        return ResponseEntity.status(HttpStatus.CREATED).body(FileResponse(fileModel))
    }

    @RequestMapping(value = ["/{uuid}"], method = [RequestMethod.GET])
    @ResponseBody
    fun download(
            @PathVariable("uuid") uuid: String
    ): FileSystemResource {
        val user = MyContextUtil.getUserOrNull()
        return fileWorker.getFileToDownload(user, uuid)
    }
}