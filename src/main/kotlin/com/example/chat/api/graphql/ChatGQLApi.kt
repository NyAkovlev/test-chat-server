package com.example.chat.api.graphql

import com.example.chat.domain.model.api.ChatResponse
import com.example.chat.domain.model.api.ChatRoleResponse
import com.example.chat.domain.model.api.ChatUserResponse
import com.example.chat.domain.worker.ChatWorker
import com.example.chat.util.MyContextUtil
import io.leangen.graphql.annotations.GraphQLContext
import io.leangen.graphql.annotations.GraphQLMutation
import io.leangen.graphql.annotations.GraphQLQuery
import io.leangen.graphql.spqr.spring.annotations.GraphQLApi
import org.springframework.stereotype.Service

@GraphQLApi
@Service
class ChatGQLApi(private val chatWorker: ChatWorker) {
    @GraphQLMutation
    fun createPersonalChat(secondUserId: Long): ChatResponse {
        val user = MyContextUtil.getUser()
        val chat = chatWorker.createPersonalChat(user, secondUserId)
        val name = chatWorker.getChatName(user, chat)
        return ChatResponse(chat, name)
    }

    @GraphQLMutation
    fun createChat(): ChatResponse {
        val user = MyContextUtil.getUser()
        val chat = chatWorker.createChat(user);
        val name = chatWorker.getChatName(user, chat)
        return ChatResponse(chat, name)
    }

    @GraphQLQuery
    fun membersOfChat(chatId: Long, page: Int, size: Int): ChatUserResponse.Users {
        val user = MyContextUtil.getUser()
        val users = chatWorker.getUsersOfChat(user, chatId, page, size)
        return ChatUserResponse.Users(
                users.content.map {
                    return@map ChatUserResponse(it)
                },
                users.totalPages,
        )
    }

    @GraphQLQuery
    fun members(@GraphQLContext chat: ChatResponse, page: Int, size: Int): ChatUserResponse.Users {
        val user = MyContextUtil.getUser()
        val users = chatWorker.getUsersOfChat(user, chat.id, page, size)
        return ChatUserResponse.Users(
                users.content.map {
                    return@map ChatUserResponse(it)
                },
                users.totalPages,
        )
    }

    @GraphQLQuery
    fun myChats(page: Int, size: Int): ChatResponse.Chats {
        val user = MyContextUtil.getUser()
        val chats = chatWorker.getChatsOfUser(user, page, size)
        return ChatResponse.Chats(
                chats.content.map { chat ->
                    val name = chatWorker.getChatName(user, chat)
                    return@map ChatResponse(chat, name)
                },
                chats.totalPages,
        )
    }

    @GraphQLMutation
    fun deleteChat(chatId: Long) {
        val user = MyContextUtil.getUser()
        chatWorker.deleteChat(user, chatId)
    }

    @GraphQLQuery
    fun chatRole(): List<ChatRoleResponse> {
        return chatWorker.chatPole().map(::ChatRoleResponse)
    }

    @GraphQLQuery
    fun personalChat(secondUserId: Long): ChatResponse? {
        val user = MyContextUtil.getUser()
        val chat = chatWorker.getPersonalChat(user, secondUserId) ?: return null
        val name = chatWorker.getChatName(user, chat)
        return ChatResponse(chat, name)
    }

    @GraphQLMutation
    fun changeAvatarOfChat(chatId: Long, fileId: String): ChatResponse {
        val user = MyContextUtil.getUser()
        val chat = chatWorker.setAvatar(user, chatId, fileId)
        val name = chatWorker.getChatName(user, chat)
        return ChatResponse(chat, name)
    }
}