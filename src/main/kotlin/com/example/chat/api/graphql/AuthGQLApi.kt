package com.example.chat.api.graphql

import com.example.chat.domain.model.api.AuthResponse
import com.example.chat.domain.worker.AuthWorker
import com.example.chat.domain.worker.DeviceWorker
import com.example.chat.domain.worker.TokenWorker
import io.leangen.graphql.annotations.GraphQLMutation
import io.leangen.graphql.spqr.spring.annotations.GraphQLApi
import org.springframework.stereotype.Service

@GraphQLApi
@Service
class AuthGQLApi(private val authWorker: AuthWorker, private val tokenWorker: TokenWorker, private val deviceWorker: DeviceWorker) {
    @GraphQLMutation
    fun signIn(
            username: String,
            password: String,
            deviceId: Long,
            deviceSecret: String
    ): AuthResponse {
        val device = deviceWorker.getDevice(deviceId, deviceSecret)
        val user = authWorker.getUser(username, password)
        val token = tokenWorker.createToken(user, device)
        return AuthResponse(token)
    }

    @GraphQLMutation
    fun signUp(
            username: String,
            password: String,
            deviceId: Long,
            deviceSecret: String
    ): AuthResponse {
        val device = deviceWorker.getDevice(deviceId, deviceSecret)
        val user = authWorker.createUser(username, password)
        val token = tokenWorker.createToken(user, device)
        return AuthResponse(token)
    }

    @GraphQLMutation
    fun refreshToken(
            refreshToken: String,
            deviceId: Long,
            deviceSecret: String
    ): AuthResponse {
        val device = deviceWorker.getDevice(deviceId, deviceSecret)
        val token = tokenWorker.updateToken(refreshToken, device)
        return AuthResponse(token)
    }

}