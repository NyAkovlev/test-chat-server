package com.example.chat.api.graphql

import com.example.chat.domain.model.api.DeviceResponse
import com.example.chat.domain.worker.DeviceWorker
import io.leangen.graphql.annotations.GraphQLMutation
import io.leangen.graphql.spqr.spring.annotations.GraphQLApi
import org.springframework.stereotype.Service


@GraphQLApi
@Service
class DeviceGQLApi(private val deviceWorker: DeviceWorker) {
    @GraphQLMutation
    fun createDevice(
            name: String,
            secret: String,
    ): DeviceResponse {
        val device = deviceWorker.createDevice(name, secret)
        return DeviceResponse(device)
    }
}