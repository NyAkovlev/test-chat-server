package com.example.chat.api.graphql

import com.example.chat.domain.model.api.ListResponse
import com.example.chat.domain.model.api.UserResponse
import com.example.chat.domain.worker.UserWorker
import com.example.chat.util.MyContextUtil
import io.leangen.graphql.annotations.GraphQLMutation
import io.leangen.graphql.annotations.GraphQLQuery
import io.leangen.graphql.spqr.spring.annotations.GraphQLApi
import org.springframework.stereotype.Service


@GraphQLApi
@Service
class UserGQLApi(private val userWorker: UserWorker) {

    @GraphQLQuery
    fun user(): UserResponse {
        val user = MyContextUtil.getUser()
        return UserResponse(user)
    }

    @GraphQLQuery
    fun findUser(username: String, page: Int, size: Int): ListResponse<UserResponse> {
        val user = MyContextUtil.getUser()
        val users = userWorker.findUser(user, username, page, size)
        return UserResponse.Users(users.content.map(::UserResponse), users.totalPages)
    }

    @GraphQLMutation
    fun changeAvatarOfUser(file: String): UserResponse {
        var user = MyContextUtil.getUser()
        user = userWorker.setAvatar(user, file)
        return UserResponse(user)
    }
}