package com.example.chat.api.graphql

import com.example.chat.domain.model.api.MessageResponse
import com.example.chat.domain.model.db.message.MessageContent
import com.example.chat.domain.model.socket.state.MessageSocketState
import com.example.chat.domain.worker.MessageWorker
import com.example.chat.util.MyContextUtil
import io.leangen.graphql.annotations.GraphQLMutation
import io.leangen.graphql.annotations.GraphQLQuery
import io.leangen.graphql.spqr.spring.annotations.GraphQLApi
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Service
import java.time.OffsetDateTime

@GraphQLApi
@Service
class MessageGQLApi(private val messageWorker: MessageWorker, private val simpMessagingTemplate: SimpMessagingTemplate) {
    @GraphQLQuery
    fun messagesOfChatBefore(chatId: Long, before: OffsetDateTime, size: Int): MessageResponse.Messages {
        val user = MyContextUtil.getUser()

        val messages = messageWorker.messagesOfChat(user, chatId, before, size)
        return MessageResponse.Messages(messages.content.map(::MessageResponse), messages.totalPages)
    }

    @GraphQLMutation
    fun sendMessage(
            chatId: Long,
            content: MessageContent
    ): MessageResponse {
        val user = MyContextUtil.getUser()
        val message = messageWorker.createMessage(user, chatId, content)
        simpMessagingTemplate.convertAndSend(
                "/sub/chat/$chatId",
                MessageSocketState(MessageSocketState.Case.Added, MessageResponse(message)
                )
        )
        return MessageResponse(message)
    }
}