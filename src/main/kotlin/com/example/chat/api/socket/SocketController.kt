package com.example.chat.api.socket


import com.example.chat.domain.model.api.MessageResponse
import com.example.chat.domain.model.socket.event.CreateMessageEvent
import com.example.chat.domain.model.socket.state.MessageSocketState
import com.example.chat.domain.worker.MessageWorker
import com.example.chat.util.WebSocketUtil.Companion.getUser
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Controller
import java.security.Principal


@Controller
class SocketController(
        private val simpMessagingTemplate: SimpMessagingTemplate,
        private val messageWorker: MessageWorker,
) {

    @MessageMapping("/sendMessage")
    fun sendMessage(principal: Principal, event: CreateMessageEvent) {
        val user = principal.getUser()
        val message = messageWorker.createMessage(user, event.chatId, event.content)
        simpMessagingTemplate.convertAndSend(
                "/sub/chat/${event.chatId}",
                MessageSocketState(MessageSocketState.Case.Added, MessageResponse(message)
                )
        )
    }

}