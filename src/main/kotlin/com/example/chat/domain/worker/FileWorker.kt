package com.example.chat.domain.worker

import com.example.chat.domain.model.db.file.FileModel
import com.example.chat.domain.model.db.user.UserModel
import com.example.chat.domain.repository.db.FileRepository
import com.example.chat.domain.repository.file.FileStorageRepository
import com.example.chat.error.AppError
import org.springframework.core.io.FileSystemResource
import org.springframework.stereotype.Component
import org.springframework.web.multipart.MultipartFile
import java.time.OffsetDateTime
import java.util.*

@Component
class FileWorker(private val fileStorageRepository: FileStorageRepository, private val fileRepository: FileRepository) {
    fun save(
            user: UserModel,
            file: MultipartFile,
            uuid: String,
            name: String,
    ): FileModel {
        fileStorageRepository.uploadFile(file.inputStream, uuid)
        return fileRepository.save(FileModel().apply {
            id = UUID.fromString(uuid)
            owner = user.id!!
            this.name = name
            created = OffsetDateTime.now()
        })
    }

    fun getFileToDownload(user: UserModel?, uuid: String): FileSystemResource {
        getFile(user, uuid)
        val path = fileStorageRepository.getFilePath(uuid)
        return FileSystemResource(path)
    }

    fun getFile(user: UserModel?, uuid: String): FileModel {
        try {
            return fileRepository.getOne(UUID.fromString(uuid))
        } catch (e: Throwable) {
            throw AppError.ModelNotFound("FileModel")
        }
    }
}