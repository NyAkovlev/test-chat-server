package com.example.chat.domain.worker

import com.example.chat.domain.model.db.chat.ChatPermission
import com.example.chat.domain.model.db.chat.ChatUserId
import com.example.chat.domain.model.db.chat.ChatUserModel
import com.example.chat.domain.model.db.user.UserModel
import com.example.chat.domain.repository.db.ChatUserRepository
import com.example.chat.error.AppError
import org.springframework.stereotype.Component

@Component
class ChatPermissionWorker(private val chatUserRepository: ChatUserRepository) {
    fun checkPermission(user: UserModel, chatId: Long, permission: ChatPermission? = null): ChatUserModel {
        try {
            val chatUser = chatUserRepository.getOne(ChatUserId().apply {
                this.chatId = chatId
                this.user = user.id!!
            })
            if (permission != null && !chatUser.role.permissions.contains(permission)) {
                throw AppError.HaveNotChatPermission(permission)
            }
            return chatUser
        } catch (e: Throwable) {
            throw AppError.AccessDenied()
        }
    }
}