package com.example.chat.domain.worker

import com.example.chat.config.security.MyPasswordEncoder
import com.example.chat.domain.model.db.user.Role
import com.example.chat.domain.model.db.user.Status
import com.example.chat.domain.model.db.user.UserModel
import com.example.chat.domain.repository.db.UserRepository
import com.example.chat.error.AppError
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.stereotype.Component
import java.time.OffsetDateTime


@Component
class AuthWorker(
        private val authenticationManager: AuthenticationManager,
        private val userRepository: UserRepository,
        private val myPasswordEncoder: MyPasswordEncoder,
) {

    fun createUser(username: String,
                   password: String): UserModel {
        val encodedPassword = myPasswordEncoder.encode(password)
        val securityUser = UserModel().apply {
            publicUsername = username
            this.username = username
            this.password = encodedPassword
            role = Role.USER
            status = Status.ACTIVE
            created = OffsetDateTime
                    .now()
        }

        try {
            return userRepository.save(securityUser)
        } catch (e: Throwable) {
            throw  AppError.ModelIdAlreadyExist()
        }
    }

    fun getUser(username: String, password: String): UserModel {
        val authentication = authenticationManager.authenticate(UsernamePasswordAuthenticationToken(username, password))


        if (authentication != null && authentication.isAuthenticated) {
            val user = userRepository.findByUsername(username)
            if (user != null) {
                return user
            }
        }
        throw AppError.AuthenticationRequired()
    }

}