package com.example.chat.domain.worker

import com.example.chat.config.security.token.JwtAccessTokenProvider
import com.example.chat.config.security.token.JwtRefreshTokenProvider
import com.example.chat.domain.model.db.device.DeviceModel
import com.example.chat.domain.model.db.token.TokenModel
import com.example.chat.domain.model.db.user.UserModel
import com.example.chat.domain.repository.db.TokenRepository
import com.example.chat.error.AppError
import org.springframework.stereotype.Component
import java.time.OffsetDateTime

@Component
class TokenWorker(
        private val jwtAccessTokenProvider: JwtAccessTokenProvider,
        private val jwtRefreshTokenProvider: JwtRefreshTokenProvider,
        private val tokenRepository: TokenRepository,
) {

    fun createToken(userModel: UserModel, device: DeviceModel): TokenModel {

        tokenRepository.deleteByDeviceId(device.id!!)

        val tokenModel = tokenRepository.save(TokenModel().apply {
            created = OffsetDateTime.now()
            updated = OffsetDateTime.now()
            deviceId = device.id!!
            userId = userModel.id!!
        })
        val accessToken = jwtAccessTokenProvider.createToken(tokenModel.id!!, userModel.id!!)
        val refreshToken = jwtRefreshTokenProvider.createToken(tokenModel.id!!, userModel.id!!)

        tokenModel.accessToken = accessToken
        tokenModel.refreshToken = refreshToken

        return tokenRepository.save(tokenModel)
    }

    fun updateToken(refreshToken: String, device: DeviceModel): TokenModel {
        jwtRefreshTokenProvider.validate(refreshToken)
        val parsedToken = jwtRefreshTokenProvider.parse(refreshToken)
        val tokenModel = tokenRepository.getOne(parsedToken.id)

        if (tokenModel.refreshToken != refreshToken || tokenModel.deviceId != device.id || tokenModel.deleted != null) {
            throw AppError.TokenException()
        }
        val newAccessToken = jwtAccessTokenProvider.createToken(parsedToken.id, parsedToken.userId)
        val newRefreshToken = jwtRefreshTokenProvider.createToken(parsedToken.id, parsedToken.userId)

        tokenModel.apply {
            this.accessToken = newAccessToken
            this.refreshToken = newRefreshToken
            this.updated = OffsetDateTime.now()
        }

        return tokenRepository.save(tokenModel)

    }

}