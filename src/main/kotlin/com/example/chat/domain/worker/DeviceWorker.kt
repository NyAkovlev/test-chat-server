package com.example.chat.domain.worker


import com.example.chat.config.security.MyPasswordEncoder
import com.example.chat.domain.model.db.device.DeviceModel
import com.example.chat.domain.repository.db.DeviceRepository
import com.example.chat.error.AppError
import org.springframework.stereotype.Component
import java.time.OffsetDateTime
import java.util.*

@Component()
class DeviceWorker(
        private val deviceRepository: DeviceRepository,
        private val myPasswordEncoder: MyPasswordEncoder
) {

    fun createDevice(
            name: String,
            secret: String,
    ): DeviceModel {

        var device = DeviceModel().apply {
            this.name = name
            this.secret = myPasswordEncoder.encode(secret)
            this.created = OffsetDateTime.now()
        }
        device = deviceRepository.save(device)
        return device
    }

    fun getDevice(deviceId: Long, deviceSecret: String): DeviceModel {
        val device = try {
            deviceRepository.getOne(deviceId)
        } catch (e: Throwable) {
            throw AppError.ModelNotFound("Device")
        }

        if (myPasswordEncoder.matches(deviceSecret, device.secret)) {
            return device
        } else {
            throw AppError.ModelNotFound("Device")
        }
    }
}