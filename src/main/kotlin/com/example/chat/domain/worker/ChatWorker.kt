package com.example.chat.domain.worker

import com.example.chat.domain.model.db.chat.ChatModel
import com.example.chat.domain.model.db.chat.ChatPermission
import com.example.chat.domain.model.db.chat.ChatRole
import com.example.chat.domain.model.db.chat.ChatUserModel
import com.example.chat.domain.model.db.user.UserModel
import com.example.chat.domain.repository.db.ChatRepository
import com.example.chat.domain.repository.db.ChatUserRepository
import com.example.chat.domain.repository.db.UserRepository
import com.example.chat.error.AppError
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Component
import java.time.OffsetDateTime

@Component
class ChatWorker(
        private val chatRepository: ChatRepository,
        private val chatUserRepository: ChatUserRepository,
        private val userRepository: UserRepository,
        private val fileWorker: FileWorker,
        private val permissionWorker: ChatPermissionWorker
) {
    fun createPersonalChat(user: UserModel, secondUserId: Long): ChatModel {
        val secondUser = try {
            userRepository.getOne(secondUserId)
        } catch (e: Throwable) {
            throw  AppError.ModelNotFound("UserModel")
        }
        var chat = getPersonalChat(user, secondUserId)
        if (chat != null) {
            throw  AppError.ModelIdAlreadyExist("ChatModel")
        }
        chat = ChatModel().apply {
            canInvite = false
            created = OffsetDateTime.now()
            updated = OffsetDateTime.now()
        }
        chat = chatRepository.save(chat)
        val firstChatUser = ChatUserModel().apply {
            this.user = user
            chatId = chat.id
            invited = OffsetDateTime.now()
            role = ChatRole.PERSONAL_CHAT_MEMBER
        }
        val secondChatUser = ChatUserModel().apply {
            this.user = secondUser
            chatId = chat.id
            invited = OffsetDateTime.now()
            role = ChatRole.PERSONAL_CHAT_MEMBER
        }
        chatUserRepository.save(firstChatUser)
        chatUserRepository.save(secondChatUser)
        return chat
    }

    fun createChat(user: UserModel): ChatModel {
        var chat = ChatModel().apply {
            canInvite = true
            created = OffsetDateTime.now()
            updated = OffsetDateTime.now()
        }
        chat = chatRepository.save(chat)
        val firstChatUser = ChatUserModel().apply {
            this.user = user
            chatId = chat.id
            invited = OffsetDateTime.now()
            role = ChatRole.PERSONAL_CHAT_MEMBER
        }
        chatUserRepository.save(firstChatUser)
        return chat
    }

    fun getChatName(user: UserModel, chat: ChatModel): String {
        if (chat.name != null) {
            return chat.name!!
        }
        val users = getUsersOfChat(user, chat.id!!, 0, 3)
        return users.map {
            it.user.publicUsername
        }.joinToString(separator = ", ")
    }

    fun getChatsOfUser(user: UserModel, page: Int, size: Int): Page<ChatModel> {
        return chatRepository.chatByUser(user.id!!, PageRequest.of(page, size))
    }

    fun getPersonalChat(user: UserModel, secondUserId: Long): ChatModel? {
        return chatRepository.personalChatByTwoUsers(user.id!!, secondUserId)
    }

    fun getUsersOfChat(user: UserModel, chatId: Long, page: Int, size: Int): Page<ChatUserModel> {
        permissionWorker.checkPermission(user, chatId)
        return chatUserRepository.usersByChatIdWithoutId(chatId, user.id!!, PageRequest.of(page, size))
    }

    fun deleteChat(user: UserModel, chatId: Long) {
        permissionWorker.checkPermission(user, chatId, ChatPermission.DELETE)
        val chat = chatRepository.getOne(chatId)
        chat.deleted = OffsetDateTime.now()
        chatRepository.save(chat)
    }

    fun chatPole(): Array<ChatRole> {
        return ChatRole.values()
    }

    fun setAvatar(user: UserModel, chatId: Long, fileId: String): ChatModel {
        permissionWorker.checkPermission(user, chatId, ChatPermission.SET_AVATAR)
        var chat = chatRepository.getOne(chatId)
        val file = fileWorker.getFile(user, fileId)
        chat.avatar = file.id
        chat = chatRepository.save(chat)
        return chat
    }
}