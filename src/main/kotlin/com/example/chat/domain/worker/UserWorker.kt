package com.example.chat.domain.worker

import com.example.chat.domain.model.db.user.UserModel
import com.example.chat.domain.repository.db.UserRepository
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Component

@Component
class UserWorker(private val userRepository: UserRepository, private val fileWorker: FileWorker) {
    fun findUser(user: UserModel, username: String, page: Int, size: Int): Page<UserModel> {
        return userRepository.findByUsernameContaining(
                user.id!!,
                username,
                PageRequest.of(page, size)
        )
    }

    fun setAvatar(user: UserModel, fileId: String): UserModel {
        val file = fileWorker.getFile(user, fileId)
        user.avatar = file.id
        return userRepository.save(user)
    }
}
