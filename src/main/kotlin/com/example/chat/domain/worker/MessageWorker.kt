package com.example.chat.domain.worker

import com.example.chat.domain.model.db.chat.ChatPermission
import com.example.chat.domain.model.db.message.MessageContent
import com.example.chat.domain.model.db.message.MessageModel
import com.example.chat.domain.model.db.user.UserModel
import com.example.chat.domain.repository.db.MessageRepository
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Component
import java.time.OffsetDateTime

@Component
class MessageWorker(
        private val messageRepository: MessageRepository,
        private val permissionWorker: ChatPermissionWorker
) {
    fun createMessage(user: UserModel, chatId: Long, messageContent: MessageContent): MessageModel {
        permissionWorker.checkPermission(user, chatId, ChatPermission.WRITE)
        var message = MessageModel().apply {
            this.chatId = chatId
            sender = user
            created = OffsetDateTime.now()
            content = messageContent
        }
        message = messageRepository.save(message)
        return message
    }

    fun messagesOfChat(user: UserModel, chatId: Long, before: OffsetDateTime, size: Int): Page<MessageModel> {
        permissionWorker.checkPermission(user, chatId, ChatPermission.READ)
        return messageRepository.messagesOfChatBeforeDate(
                chatId,
                before,
                PageRequest.of(0, size)
        )
    }

}