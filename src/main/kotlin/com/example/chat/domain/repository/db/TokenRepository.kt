package com.example.chat.domain.repository.db

import com.example.chat.domain.model.db.token.TokenModel
import org.springframework.data.jpa.repository.JpaRepository
import javax.transaction.Transactional

interface TokenRepository : JpaRepository<TokenModel, Long> {
    @Transactional
    fun deleteByDeviceId(deviceId: Long): Int
}
