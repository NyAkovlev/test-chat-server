package com.example.chat.domain.repository.db

import com.example.chat.domain.model.db.user.UserModel
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface UserRepository : JpaRepository<UserModel, Long> {
    fun findByUsername(username: String): UserModel?

    @Query(
            "SELECT u.* FROM users u WHERE u.id != ?1 AND (u.username LIKE %?2% OR u.public_username LIKE %?2%)",
            countQuery = "SELECT count(*) FROM users",
            nativeQuery = true
    )
    fun findByUsernameContaining(
            withOutId: Long,
            username: String,
            pageable: Pageable
    ): Page<UserModel>
}