package com.example.chat.domain.repository.db

import com.example.chat.domain.model.db.device.DeviceModel
import org.springframework.data.jpa.repository.JpaRepository

interface DeviceRepository : JpaRepository<DeviceModel, Long> {

}
