package com.example.chat.domain.repository.db

import com.example.chat.domain.model.db.message.MessageModel
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import java.time.OffsetDateTime


interface MessageRepository : JpaRepository<MessageModel, Long> {
    @Query(
            value = "SELECT me.* FROM messages me WHERE me.chat_id = ?1 and me.created < ?2 ORDER BY me.created DESC",
            countQuery = "SELECT count(*) FROM messages",
            nativeQuery = true
    )
    fun messagesOfChatBeforeDate(chatId: Long, date: OffsetDateTime, pageable: Pageable): Page<MessageModel>
}
