package com.example.chat.domain.repository.db

import com.example.chat.domain.model.db.chat.ChatModel
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query


interface ChatRepository : JpaRepository<ChatModel, Long> {
    @Query(
            value = "SELECT c.* FROM chats c LEFT JOIN users_of_chats uc ON uc.chat_id= c.id where uc.user_id = ?1 AND c.deleted IS NULL ORDER BY c.updated",
            countQuery = "SELECT count(*) FROM chats",
            nativeQuery = true
    )
    fun chatByUser(userId: Long, pageable: Pageable): Page<ChatModel>

    @Query(
            value = "SELECT c.* FROM chats c LEFT JOIN users_of_chats uc1 ON uc1.chat_id = c.id LEFT JOIN users_of_chats uc2 ON uc2.chat_id=c.id WHERE uc1.user_id= ?1 AND uc2.user_id = ?2 AND c.can_invite = false AND c.deleted IS NULL",
            nativeQuery = true
    )
    fun personalChatByTwoUsers(firstUserId: Long, secondUserId: Long): ChatModel?
}
