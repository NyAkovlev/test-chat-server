package com.example.chat.domain.repository.file

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import org.springframework.util.StringUtils
import java.io.File
import java.io.InputStream
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.StandardCopyOption

@Component
class FileStorageRepository {
    @Value("\${app.upload.dir}")
    var uploadDir: String? = null

    fun uploadFile(input: InputStream, uuid: String) {
        try {
            val copyLocation = Paths
                    .get(uploadDir + File.separator + StringUtils.cleanPath(uuid))
            Files.copy(input, copyLocation, StandardCopyOption.REPLACE_EXISTING)
        } catch (e: Exception) {
            e.printStackTrace()
            throw Error("Could not store file $uuid. Please try again!")
        }
    }

    fun getFilePath(uuid: String): Path {
        return Paths
                .get(uploadDir + File.separator + StringUtils.cleanPath(uuid))
    }
}