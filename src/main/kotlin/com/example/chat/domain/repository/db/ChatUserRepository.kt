package com.example.chat.domain.repository.db

import com.example.chat.domain.model.db.chat.ChatUserId
import com.example.chat.domain.model.db.chat.ChatUserModel
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query


interface ChatUserRepository : JpaRepository<ChatUserModel, ChatUserId> {
    @Query(
            value = "SELECT uc.* FROM users_of_chats uc WHERE uc.chat_id = ?1 and uc.user_id != ?2 ORDER BY uc.invited",
            countQuery = "SELECT count(*) FROM users_of_chats",
            nativeQuery = true
    )
    fun usersByChatIdWithoutId(chatId: Long, withoutUserId: Long, pageable: Pageable): Page<ChatUserModel>
    @Query(
            value = "SELECT uc.* FROM users_of_chats uc WHERE uc.chat_id = ?1 and uc.user_id != ?2 ORDER BY uc.invited",
            countQuery = "SELECT count(*) FROM users_of_chats",
            nativeQuery = true
    )
    fun usersByChatId(chatId: Long, pageable: Pageable): Page<ChatUserModel>
}
