package com.example.chat.domain.repository.db

import com.example.chat.domain.model.db.file.FileModel
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*


interface FileRepository : JpaRepository<FileModel, UUID> {

}
