package com.example.chat.domain.repository.db

import org.springframework.data.jpa.repository.support.JpaEntityInformation
import org.springframework.data.jpa.repository.support.SimpleJpaRepository
import javax.persistence.EntityManager
import javax.transaction.Transactional


class MyRepositoryImpl<T, ID>(
        @Suppress("SpringJavaInjectionPointsAutowiringInspection")
        private val entityInformation: JpaEntityInformation<T, ID>,
        private val entityManager: EntityManager
) : SimpleJpaRepository<T, ID>(entityInformation, entityManager) {

    @Transactional
    override fun <S : T> save(entity: S): S {
        return if (entityInformation.isNew(entity)) {
            entityManager.persist(entity)
            entity
        } else {
            entityManager.merge(entity)
        }
    }
}