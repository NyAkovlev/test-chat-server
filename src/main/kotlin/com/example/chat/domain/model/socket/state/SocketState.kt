package com.example.chat.domain.model.socket.state

abstract class SocketState<T>(val name: String, val model: T)