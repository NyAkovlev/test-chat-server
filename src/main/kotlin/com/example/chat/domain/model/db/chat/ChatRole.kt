package com.example.chat.domain.model.db.chat

import io.leangen.graphql.annotations.types.GraphQLType
import org.springframework.security.core.authority.SimpleGrantedAuthority
import java.util.stream.Collectors

@GraphQLType(description = "")
enum class ChatRole(val permissions: Set<ChatPermission>) {
    MEMBER(setOf(
            ChatPermission.LEAVE,
            ChatPermission.READ,
            ChatPermission.WRITE,
    )),
    PERSONAL_CHAT_MEMBER(setOf(
            ChatPermission.READ,
            ChatPermission.WRITE,
    )),
    HOST(setOf(
            ChatPermission.SET_AVATAR,
            ChatPermission.KICK,
            ChatPermission.INVITE,
            ChatPermission.DELETE,
            ChatPermission.RENAME,
            ChatPermission.READ,
            ChatPermission.WRITE,
    ));

    val authorities: Set<SimpleGrantedAuthority>
        get() = permissions.stream()
                .map { SimpleGrantedAuthority(it.name) }
                .collect(Collectors.toSet())

}