package com.example.chat.domain.model.db.chat

enum class ChatPermission {
    DELETE,
    INVITE,
    KICK,
    LEAVE,
    WRITE,
    READ,
    RENAME,
    SET_AVATAR;

}