package com.example.chat.domain.model.api

import com.example.chat.domain.model.db.token.TokenModel

class AuthResponse(
        val accessToken: String,
        val refreshToken: String
) {

    constructor(model: TokenModel) : this(
            model.accessToken!!,
            model.refreshToken!!,
    )
}