package com.example.chat.domain.model.socket.state

import com.example.chat.domain.model.api.ChatResponse
import com.example.chat.domain.model.api.MessageResponse

class MessageSocketState(
        case: Case,
        model: MessageResponse
) : SocketState<MessageResponse>(
        case.name, model
) {
    enum class Case {
        Added,
        Removed,
        Edited,
    }
}