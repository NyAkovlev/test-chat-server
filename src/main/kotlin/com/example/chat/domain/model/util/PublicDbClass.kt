package com.example.chat.domain.model.util

// по умолчанию запрешено указывать модели базы данных в ответе api,
// но данная аннотация позволяет отключить эту проверку для помеченного класса
@Target(AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class PublicDbClass()