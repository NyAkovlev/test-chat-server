package com.example.chat.domain.model.api

import com.example.chat.domain.model.db.file.FileModel
import java.time.OffsetDateTime

class FileResponse(val id: String, val name: String, val owner: Long, val created: OffsetDateTime) {
    constructor(model: FileModel) : this(model.id.toString(), model.name, model.owner, model.created)
}