package com.example.chat.domain.model.db.user

enum class Permission {
    VIEW_USERS,
    VIEW_CHAT,
    CREATE_CHAT,
    VIEW_MESSAGE,
    CREATE_MESSAGE;
}