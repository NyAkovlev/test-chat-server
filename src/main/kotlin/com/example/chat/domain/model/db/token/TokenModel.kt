package com.example.chat.domain.model.db.token

import java.time.OffsetDateTime
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "tokens")
class TokenModel {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    @Column(name = "created")
    lateinit var created: OffsetDateTime


    @Column(name = "updated")
    lateinit var updated: OffsetDateTime


    @Column(name = "device_id")
    var deviceId: Long = -1

    @Column(name = "user_id")
    var userId: Long = -1

    @Column(name = "access_token")
    var accessToken: String? = null

    @Column(name = "refresh_token")
    var refreshToken: String? = null

    @Column(name = "deleted")
    var deleted: OffsetDateTime? = null
}
