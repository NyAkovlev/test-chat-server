package com.example.chat.domain.model.db.chat

import java.time.OffsetDateTime
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "chats")
class ChatModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    @Column(name = "name")
    var name: String? = null

    @Column(name = "avatar")
    var avatar: UUID? = null

    @Column(name = "can_invite")
    var canInvite: Boolean = false

    @Column(name = "created")
    lateinit var created: OffsetDateTime


    @Column(name = "updated")
    lateinit var updated: OffsetDateTime


    @Column(name = "deleted")
    var deleted: OffsetDateTime
    ? = null
}