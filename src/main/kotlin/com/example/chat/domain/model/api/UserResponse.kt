package com.example.chat.domain.model.api

import com.example.chat.domain.model.db.user.UserModel
import io.leangen.graphql.annotations.GraphQLNonNull

class UserResponse(
        val id: @GraphQLNonNull Long,
        val publicUsername: String,
        val username: String,
        val avatar: String?,
) {

    constructor(model: UserModel) : this(
            model.id!!,
            model.publicUsername,
            model.username,
            model.avatar?.toString(),
    )

    class Users(items: List<UserResponse>, totalPages: Int) : ListResponse<UserResponse>(items, totalPages)
}
