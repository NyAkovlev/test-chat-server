package com.example.chat.domain.model.api

import com.example.chat.domain.model.db.chat.ChatRole

class ChatRoleResponse(val name: String, val permission: List<String>) {
    constructor(model: ChatRole) : this(
            model.name,
            model.permissions.map { it.name },
    )
}