package com.example.chat.domain.model.db.message

import com.example.chat.domain.model.db.user.UserModel
import com.vladmihalcea.hibernate.type.json.JsonBinaryType
import org.hibernate.annotations.Type
import org.hibernate.annotations.TypeDef
import java.time.OffsetDateTime
import javax.persistence.*

@Entity
@Table(name = "messages")
@TypeDef(name = "jsonb", typeClass = JsonBinaryType::class)
class MessageModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    @JoinColumn(name = "sender")
    @ManyToOne(fetch = FetchType.LAZY)
    lateinit var sender: UserModel

    @Column(name = "chat_id")
    var chatId: Long = -1

    @Column(name = "created")
    lateinit var created: OffsetDateTime


    @Type(type = "jsonb")
    @Column(name = "content")
    lateinit var content: MessageContent
}