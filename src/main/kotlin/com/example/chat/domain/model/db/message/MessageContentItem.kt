package com.example.chat.domain.model.db.message

class MessageContentItem(val name: String,
                         val value: String)