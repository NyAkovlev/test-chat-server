package com.example.chat.domain.model.db.chat

import com.example.chat.domain.model.db.user.UserModel
import java.time.OffsetDateTime
import javax.persistence.*

@Entity
@IdClass(ChatUserId::class)
@Table(name = "users_of_chats")
class ChatUserModel {
    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    lateinit var user: UserModel

    @Id
    @Column(name = "chat_id")
    var chatId: Long? = null

    @Column(name = "invited")
    lateinit var invited: OffsetDateTime

    @Enumerated(value = EnumType.STRING)
    @Column(name = "role")
    lateinit var role: ChatRole
}