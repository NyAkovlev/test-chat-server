package com.example.chat.domain.model.api

import com.example.chat.domain.model.db.message.MessageContent
import com.example.chat.domain.model.db.message.MessageModel
import java.time.OffsetDateTime

class MessageResponse(
        val id: Long,
        val chatId: Long,
        val content: MessageContent,
        val created: OffsetDateTime,
        val sender: Long
) {
    constructor(model: MessageModel) : this(
            model.id!!,
            model.chatId,
            model.content,
            model.created,
            model.sender.id!!
    )

    class Messages(items: List<MessageResponse>, totalPages: Int) : ListResponse<MessageResponse>(items, totalPages)
}