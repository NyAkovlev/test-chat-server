package com.example.chat.domain.model.api

import com.example.chat.domain.model.db.chat.ChatRole
import com.example.chat.domain.model.db.chat.ChatUserModel
import java.time.OffsetDateTime
import java.util.*

class ChatUserResponse(val user: UserResponse, val role: ChatRole, val invited: OffsetDateTime) {
    constructor(model: ChatUserModel) : this(
            UserResponse(model.user),
            model.role,
            model.invited,
    )

    class Users(items: List<ChatUserResponse>, totalPages: Int) : ListResponse<ChatUserResponse>(items, totalPages)
}