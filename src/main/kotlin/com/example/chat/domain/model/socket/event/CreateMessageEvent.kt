package com.example.chat.domain.model.socket.event

import com.example.chat.domain.model.db.message.MessageContent

class CreateMessageEvent(
        val chatId: Long,
        val content: MessageContent,
)