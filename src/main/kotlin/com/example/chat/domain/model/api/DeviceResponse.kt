package com.example.chat.domain.model.api

import com.example.chat.domain.model.db.device.DeviceModel

class DeviceResponse(val id: Long, val name: String) {
    constructor(model: DeviceModel) : this(
            model.id!!,
            model.name
    )
}