package com.example.chat.domain.model.db.user

import java.time.OffsetDateTime
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "users")
class UserModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    @Column(name = "username", unique = true)
    lateinit var username: String

    @Column(name = "public_username")
    lateinit var publicUsername: String

    @Column(name = "password")
    lateinit var password: String

    @Enumerated(value = EnumType.STRING)
    @Column(name = "role")
    lateinit var role: Role

    @Enumerated(value = EnumType.STRING)
    @Column(name = "status")
    lateinit var status: Status

    @Column(name = "created")
    lateinit var created: OffsetDateTime

    @Column(name = "avatar")
    var avatar: UUID? = null
}