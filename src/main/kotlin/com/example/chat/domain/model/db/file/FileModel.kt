package com.example.chat.domain.model.db.file

import org.hibernate.annotations.Type
import java.time.OffsetDateTime
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "files")
class FileModel {
    @Id
    lateinit var id: UUID

    @Column(name = "name")
    lateinit var name: String

    @Column(name = "owner")
    var owner: Long = -1

    @Column(name = "created")
    lateinit var created: OffsetDateTime
}