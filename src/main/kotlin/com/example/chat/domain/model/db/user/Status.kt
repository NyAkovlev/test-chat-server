package com.example.chat.domain.model.db.user

enum class Status {
    ACTIVE, BANNED
}