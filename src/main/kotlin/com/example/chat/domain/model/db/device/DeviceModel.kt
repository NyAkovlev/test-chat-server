package com.example.chat.domain.model.db.device

import java.time.OffsetDateTime
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "devices")
class DeviceModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    @Column(name = "name")
    lateinit var name: String

    @Column(name = "secret")
    lateinit var secret: String

    @Column(name = "created")
    lateinit var created: OffsetDateTime



}