package com.example.chat.domain.model.api

import com.example.chat.domain.model.db.chat.ChatModel
import java.time.OffsetDateTime

class ChatResponse(
        val id: Long,
        val name: String,
        val avatar: String?,
        val canInvite: Boolean,
        val created: OffsetDateTime,
        val updated: OffsetDateTime,
        val deleted: OffsetDateTime?
) {
    constructor(model: ChatModel, nameFromUsers: String) : this(
            model.id!!,
            model.name ?: nameFromUsers,
            model.avatar?.toString(),
            model.canInvite,
            model.created,
            model.updated,
            model.deleted,
    )

    class Chats(items: List<ChatResponse>, totalPages: Int) : ListResponse<ChatResponse>(items, totalPages)
}