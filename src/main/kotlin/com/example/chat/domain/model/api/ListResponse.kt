package com.example.chat.domain.model.api
import org.springframework.data.domain.Page

abstract class ListResponse<T>(val items: List<T>, val totalPages: Int)