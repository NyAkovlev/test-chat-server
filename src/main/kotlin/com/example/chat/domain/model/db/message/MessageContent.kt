package com.example.chat.domain.model.db.message

class MessageContent(val items: List<MessageContentItem>)