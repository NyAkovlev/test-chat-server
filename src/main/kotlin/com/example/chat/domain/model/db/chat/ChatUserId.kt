package com.example.chat.domain.model.db.chat

import java.io.Serializable
import javax.persistence.Embeddable


@Embeddable
class ChatUserId(
        var chatId: Long = -1,
        var user: Long = -1
) : Serializable