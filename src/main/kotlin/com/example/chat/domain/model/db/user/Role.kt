package com.example.chat.domain.model.db.user

import io.leangen.graphql.annotations.types.GraphQLType
import org.springframework.security.core.authority.SimpleGrantedAuthority
import java.util.stream.Collectors

@GraphQLType(description = "")
enum class Role(val permissions: Set<Permission>) {
    USER(setOf(
            Permission.CREATE_CHAT,
            Permission.CREATE_MESSAGE,
            Permission.VIEW_USERS,
            Permission.VIEW_CHAT,
            Permission.VIEW_MESSAGE
    ));

    val authorities: Set<SimpleGrantedAuthority>
        get() = permissions.stream()
                .map { SimpleGrantedAuthority(it.name) }
                .collect(Collectors.toSet())

}