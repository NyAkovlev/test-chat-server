package com.example.chat.config

import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.PropertySource

@Configuration
@PropertySource("classpath:security.properties")
class PropertiesConfig {
}