package com.example.chat.config.mapper

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.springframework.context.annotation.Bean

@Bean
fun objectMapper(): ObjectMapper {
    return jacksonObjectMapper();
}