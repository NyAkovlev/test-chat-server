package com.example.chat.config.wrapper

import com.example.chat.error.AppError
import graphql.GraphQLException
import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.Around
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.annotation.Pointcut
import org.springframework.stereotype.Component


@Aspect
@Component
class GraphQlExceptionWrap {
    @Pointcut("@annotation(io.leangen.graphql.annotations.GraphQLMutation)")
    private fun graphqlMutation() {
    }

    @Pointcut("@annotation(io.leangen.graphql.annotations.GraphQLQuery)")
    private fun graphqlQuery() {
    }

    @Around("graphqlMutation() || graphqlQuery()")
    fun wrapException(jp: ProceedingJoinPoint): Any {
        return try {
            jp.proceed()
        } catch (throwable: Throwable) {
            if (throwable is AppError) {
                throw throwable.toGraphQL()
            } else {
                throw GraphQLException(throwable)
            }

        }
    }
}