package com.example.chat.config.socket

import org.springframework.messaging.simp.SimpMessageHeaderAccessor
import org.springframework.stereotype.Component
import org.springframework.web.socket.WebSocketSession

@Component
class SocketSessionHolder {
    private val listeners = mutableListOf<(WebSocketSession) -> Unit>()
    private val sessions = mutableMapOf<String, WebSocketSession>()

    fun getCurrent(accessor: SimpMessageHeaderAccessor): WebSocketSession? {
        val sessionId: String = accessor.sessionId ?: return null
        return sessions[sessionId]
    }

    fun add(session: WebSocketSession) {
        sessions[session.id] = session
    }

    fun close(session: WebSocketSession) {
        sessions.remove(session.id)
        for (listener in listeners) {
            try {
                listener.invoke(session)
            } catch (e: Throwable) {
                print(e)
            }
        }
    }

    fun addDisconnectListener(onDisconnect: (WebSocketSession) -> Unit) {
        listeners.add(onDisconnect)
    }
}