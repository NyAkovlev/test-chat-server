package com.example.chat.config.socket

import org.springframework.context.annotation.Configuration
import org.springframework.messaging.simp.config.MessageBrokerRegistry
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker
import org.springframework.web.socket.config.annotation.StompEndpointRegistry
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer
import org.springframework.web.socket.config.annotation.WebSocketTransportRegistration


@Configuration
@EnableWebSocketMessageBroker
class MyWebSocketMessageBrokerConfig(private val sessionHolder: SocketSessionHolder) : WebSocketMessageBrokerConfigurer {
    override fun configureWebSocketTransport(registry: WebSocketTransportRegistration) {
        registry.addDecoratorFactory { handler ->
            MyWebSocketHandlerDecorator(sessionHolder, handler)
        }
        super.configureWebSocketTransport(registry)
    }

    override fun configureMessageBroker(config: MessageBrokerRegistry) {
        config.enableSimpleBroker("/sub")
        config.setApplicationDestinationPrefixes("/send")
        config.setUserDestinationPrefix("/sub/secured")
    }

    override fun registerStompEndpoints(registry: StompEndpointRegistry) {
        registry.addEndpoint("/ws")
                //todo
                .setAllowedOrigins("*")
                .withSockJS()

    }

}