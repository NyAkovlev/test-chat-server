package com.example.chat.config.socket

import org.springframework.web.socket.CloseStatus
import org.springframework.web.socket.WebSocketHandler
import org.springframework.web.socket.WebSocketSession
import org.springframework.web.socket.handler.WebSocketHandlerDecorator

class MyWebSocketHandlerDecorator(private val sessionHolder: SocketSessionHolder, delegate: WebSocketHandler)
    : WebSocketHandlerDecorator(delegate) {
    override fun afterConnectionEstablished(session: WebSocketSession) {
        sessionHolder.add(session)
        super.afterConnectionEstablished(session)
    }

    override fun afterConnectionClosed(session: WebSocketSession, closeStatus: CloseStatus) {
        sessionHolder.close(session)
        super.afterConnectionClosed(session, closeStatus)
    }
}