package com.example.chat.config.validator

import com.example.chat.domain.model.util.PublicDbClass
import graphql.schema.GraphQLObjectType
import graphql.schema.GraphQLSchema
import org.springframework.stereotype.Component
import java.lang.reflect.AnnotatedType
import java.util.*
import javax.persistence.Table

@Component
class GraphQlSchemaValidator(schema: GraphQLSchema) {

    init {
        schema.typeMap.values.forEach {
            if (it is GraphQLObjectType) {
                it.directives.forEach { directive ->
                    directive.arguments.forEach { argument ->
                        val value = argument.value
                        if (value is AnnotatedType) {
                            val type = value.type
                            if (type is Class<*>) {
                                val hasTable = type.annotations.firstOrNull { annotation ->
                                    return@firstOrNull annotation.annotationClass.java == Table::class.java
                                } != null
                                if (hasTable) {
                                    val hasPublic = type.annotations.firstOrNull { annotation ->
                                        return@firstOrNull annotation.annotationClass.java == PublicDbClass::class.java
                                    } != null
                                    if (!hasPublic) {
                                        throw Error("Don't use DBModel in graphql's scheme ")
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}