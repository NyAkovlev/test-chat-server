package com.example.chat.config.security.token

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

@Component
class JwtAccessTokenProvider(
        @Value("\${jwt.accessExpiration}") validityInSecond: Long,
        @Value("\${jwt.accessSecret}") secret: String,
) : JwtTokenProvider(secret, validityInSecond) {
}