package com.example.chat.config.security.token

import com.example.chat.error.AppError
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import java.util.*


abstract class JwtTokenProvider(
        private val secret: String,
        private val validityInSecond: Long
) {

    private val jwtParser = Jwts.parser().setSigningKey(secret)

    fun createToken(
            tokenId: Long,
            userId: Long,
    ): String {
        val claims = Jwts.claims()
        claims["id"] = tokenId
        claims["userId"] = userId
        val now = Date()
        val validity = Date(now.time + validityInSecond * 1000)
        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(validity)
                .signWith(SignatureAlgorithm.HS256, secret)
                .compact()
    }

    fun parse(token: String): ParsedToken {
        val claims = jwtParser.parseClaimsJws(token)
        return ParsedToken(
                (claims.body["id"] as Number).toLong(),
                (claims.body["userId"] as Number).toLong(),
                claims.body.expiration,
                claims.body.expiration.after(Date())
        )
    }

    fun validate(token: String) {
        try {
            val claims = jwtParser.parseClaimsJws(token)
            if (claims.body.expiration.before(Date())) {
                throw AppError.TokenException()
            }
        } catch (e: Throwable) {
            throw AppError.TokenException()
        }
    }

}