package com.example.chat.config.security

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Component

@Component()
class MyPasswordEncoder : BCryptPasswordEncoder(12)