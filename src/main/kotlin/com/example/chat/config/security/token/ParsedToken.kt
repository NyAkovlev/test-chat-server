package com.example.chat.config.security.token

import java.util.*

class ParsedToken(
        val id: Long,
        val userId: Long,
        val expiration: Date,
        val valid: Boolean
)