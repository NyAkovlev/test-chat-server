package com.example.chat.config.security.token

import com.example.chat.config.security.auth.UserDetailsServiceImpl
import com.example.chat.domain.repository.db.TokenRepository
import com.example.chat.error.AppError
import com.example.chat.util.MyContextUtil.Companion.setAuthentication
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Component
import org.springframework.web.filter.GenericFilterBean
import javax.servlet.FilterChain
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class JwtTokenFilter(
        private val userDetailsService: UserDetailsServiceImpl,
        private val jwtAccessTokenProvider: JwtAccessTokenProvider,
        private val tokenRepository: TokenRepository
) : GenericFilterBean() {

    override fun doFilter(servletRequest: ServletRequest, servletResponse: ServletResponse, filterChain: FilterChain) {
        val token = resolveToken(servletRequest as HttpServletRequest)
        if (token != null) {
            try {
                validateToken(token)
                getAuthentication(token)?.let { authentication ->
                    servletRequest.session.setAuthentication(authentication)
                    SecurityContextHolder.getContext().authentication = authentication
                }
            } catch (e: Throwable) {
                SecurityContextHolder.clearContext()
                servletRequest.session.setAuthentication(null)
                (servletResponse as HttpServletResponse).sendError(401)
                if (e is AppError.TokenException) {
                    throw e.toGraphQL()
                } else {
                    throw AppError.TokenException().toGraphQL(e)
                }
            }
        }
        filterChain.doFilter(servletRequest, servletResponse)
    }

    private fun validateToken(token: String) {
        jwtAccessTokenProvider.validate(token)
        val parsedToken = jwtAccessTokenProvider.parse(token)
        val tokenModel = tokenRepository.getOne(parsedToken.id)
        if (tokenModel.deleted != null) {
            throw AppError.TokenException()
        }
        if (tokenModel.accessToken != token) {
            throw AppError.TokenException()
        }
    }

    private fun getAuthentication(token: String): Authentication? {
        return try {
            val userId = jwtAccessTokenProvider.parse(token).userId
            val userDetails = userDetailsService.loadUserById(userId)
            UsernamePasswordAuthenticationToken(userDetails, "", userDetails.authorities)
        } catch (e: UsernameNotFoundException) {
            null
        }
    }

    private fun resolveToken(request: HttpServletRequest): String? {
        val header = request.getHeader(authorization)
        if (header?.startsWith(bearer) == true) {
            return header.substring(bearer.length);
        }
        return null
    }

    companion object {
        private const val bearer = "Bearer "
        private const val authorization = "Authorization"
    }
}