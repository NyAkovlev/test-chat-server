package com.example.chat.config.security.auth

import com.example.chat.domain.model.db.user.Status
import com.example.chat.domain.model.db.user.UserModel

class SecurityUserDetails(val userModel: UserModel) : org.springframework.security.core.userdetails.User(
        userModel.username, userModel.password,
        userModel.status == Status.ACTIVE,
        userModel.status == Status.ACTIVE,
        userModel.status == Status.ACTIVE,
        userModel.status == Status.ACTIVE,
        userModel.role.authorities,
)