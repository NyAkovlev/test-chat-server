package com.example.chat.config.security.auth

import com.example.chat.domain.model.db.user.UserModel
import com.example.chat.domain.repository.db.UserRepository
import com.example.chat.error.AppError
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service

@Service("userDetailsServiceImpl")
class UserDetailsServiceImpl(private val userRepository: UserRepository) : UserDetailsService {

    override fun loadUserByUsername(email: String): UserDetails {
        val userModel: UserModel = userRepository.findByUsername(email) ?: throw  AppError.ModelNotFound("UserModel")
        return SecurityUserDetails(userModel)
    }

    fun loadUserById(id: Long): UserDetails {
        val userModel: UserModel = userRepository.findById(id).orElseThrow {
            throw  AppError.ModelNotFound("UserModel")
        }
        return SecurityUserDetails(userModel)
    }
}