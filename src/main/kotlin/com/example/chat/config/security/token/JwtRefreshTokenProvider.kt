package com.example.chat.config.security.token

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

@Component
class JwtRefreshTokenProvider(
        @Value("\${jwt.refreshExpiration}") validityInSecond: Long,
        @Value("\${jwt.refreshSecret}") secret: String,
) : JwtTokenProvider(secret, validityInSecond)