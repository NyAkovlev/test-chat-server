package com.example.chat.config.security.auth

import com.example.chat.config.security.MyPasswordEncoder
import com.example.chat.domain.repository.db.UserRepository
import com.example.chat.error.AppError
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Component

@Component
class MyAuthenticationProvider(
        private val userRepository: UserRepository,
        private val myPasswordEncoder: MyPasswordEncoder
) : AuthenticationProvider {


    override fun authenticate(authentication: Authentication): Authentication {
        val upAuth = authentication as UsernamePasswordAuthenticationToken
        val name = authentication.getPrincipal() as String
        val password = upAuth.credentials as String
        val storedPassword: String = userRepository.findByUsername(name)?.password
                ?: throw AppError.InvalidAuth()

        if (password == "" || !myPasswordEncoder.matches(password, storedPassword)) {
            throw throw AppError.InvalidAuth()
        }
        val principal = authentication.getPrincipal()
        val result = UsernamePasswordAuthenticationToken(
                principal, authentication.getCredentials(), emptyList())
        result.details = authentication.getDetails()
        return result
    }

    override fun supports(aClass: Class<*>?): Boolean {
        return true
    }
}