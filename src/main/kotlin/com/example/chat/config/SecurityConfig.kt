package com.example.chat.config

import com.example.chat.config.security.auth.MyAuthenticationProvider
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
class SecurityConfig(
        private val authProvider: MyAuthenticationProvider,
        private val jwtConfigurer: JwtConfigurer
) : WebSecurityConfigurerAdapter() {


    override fun configure(http: HttpSecurity) {
        http
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers("/gui").permitAll()
                .antMatchers("/graphql").permitAll()
                .antMatchers("/scheme/update/**").permitAll()
                .antMatchers("/scheme/listen/**").permitAll()
                .antMatchers("/ws").permitAll()
                .anyRequest().permitAll()
                .and()
                .apply(jwtConfigurer)
        http.authenticationProvider(authProvider)
    }

    @Bean
    override fun authenticationManagerBean(): AuthenticationManager? {
        return super.authenticationManagerBean()
    }


}