CREATE TABLE public.users
(
    id bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1 ),
    username character varying COLLATE pg_catalog."default" NOT NULL,
    password character varying COLLATE pg_catalog."default" NOT NULL,
    role character varying COLLATE pg_catalog."default" NOT NULL,
    status character varying COLLATE pg_catalog."default" NOT NULL,
    public_username character varying COLLATE pg_catalog."default" NOT NULL,
    created date NOT NULL,
    CONSTRAINT secure_user_pkey PRIMARY KEY (id),
    CONSTRAINT unique_email UNIQUE (username)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

CREATE TABLE public.chats
(
    id bigint NOT NULL,
    name character varying COLLATE pg_catalog."default",
    avatar character varying COLLATE pg_catalog."default",
    can_invite boolean NOT NULL,
    created date NOT NULL,
    updated date NOT NULL,
    deleted date,
    CONSTRAINT chats_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;


CREATE TABLE public.devices
(
    id bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1 ),
    name character varying COLLATE pg_catalog."default" NOT NULL,
    secret character varying COLLATE pg_catalog."default" NOT NULL,
    created date NOT NULL,
    CONSTRAINT devices_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

CREATE TABLE public.files
(
    id bigint NOT NULL,
    path character varying COLLATE pg_catalog."default" NOT NULL,
    name character varying COLLATE pg_catalog."default",
    CONSTRAINT files_pkey PRIMARY KEY (id),
    CONSTRAINT files_path_key UNIQUE (path)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

CREATE TABLE public.tokens
(
    id bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1 ),
    access_token character varying COLLATE pg_catalog."default",
    refresh_token character varying COLLATE pg_catalog."default",
    created date NOT NULL,
    updated date NOT NULL,
    deleted date,
    user_id bigint NOT NULL,
    device_id bigint NOT NULL,
    CONSTRAINT tokens_pkey PRIMARY KEY (id),
    CONSTRAINT device_id_unique UNIQUE (device_id),
    CONSTRAINT device_id_fk FOREIGN KEY (device_id)
        REFERENCES public.devices (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT user_id_fk FOREIGN KEY (user_id)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

CREATE TABLE public.users_of_chats
(
    user_id bigint NOT NULL,
    chat_id bigint NOT NULL,
    invited date NOT NULL,
    role character varying COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT users_of_chats_pkey PRIMARY KEY (user_id, chat_id),
    CONSTRAINT users_of_chats_chat_id_fkey FOREIGN KEY (chat_id)
        REFERENCES public.chats (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT users_of_chats_user_id_fkey FOREIGN KEY (user_id)
        REFERENCES public.users (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;